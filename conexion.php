<?php

    class conexion{

        private $hostname = "172.22.0.2";
        private $username = "admin";
        private $password = "admin123";
        private $database = "stressout";
        private $conn;

        public function __construct() {
            $this->conn = new mysqli($this->hostname, $this->username, $this->password, $this->database);
            if(!$this->conn){
                echo "Fallo, revise ip o firewall";
                exit();
            }else{
                mysqli_set_charset($this->conn, 'utf8');
            }
        }

        public function getConn(){
            return $this->conn;
        }
    }
?>

